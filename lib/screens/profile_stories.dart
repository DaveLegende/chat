import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'view_image.dart';

class ProfileStories extends StatefulWidget {
  const ProfileStories({ Key key }) : super(key: key);

  @override
  _ProfileStoriesState createState() => _ProfileStoriesState();
}

class _ProfileStoriesState extends State<ProfileStories> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.arrow_back_ios)
                  ),
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.notification_important, color: Colors.blue)
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(Feather.more_horizontal, color: Colors.blue)
                      )
                    ]
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: ListTile(
                leading: CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                ),
                title: Text("Marcus Sanchez"),
                subtitle: Text("15k followers . 145 following"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text("Design for awesome esperiences and bright brands"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Container(
                      margin: EdgeInsets.only(right: 5),
                      // padding: const EdgeInsets.symmetric(horizontal: 10),
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[50].withOpacity(0.9),
                          )
                        ]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {},
                            icon: Icon(Feather.video, color: Colors.blue)
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: VerticalDivider()
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(Ionicons.ios_call, color: Colors.blue)
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: VerticalDivider()
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(EvilIcons.comment, color: Colors.blue)
                          ),
                        ]
                      )
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      margin: EdgeInsets.only(left: 5),
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(color: Colors.blue, width: 1),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.blue[50].withOpacity(0.9),
                          )
                        ]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("Following"),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(AntDesign.down, color: Colors.blue)
                          )
                        ],
                      )
                    )
                  )
                ]
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: GridView.builder(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: 20,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ViewImage(index: index)
                        )
                      );
                    },
                    child: ClipRRect(
                      child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU", 
                        fit: BoxFit.cover
                      ),
                    )
                  ),
                )
              ),
            )
          ],
        ),
      )
    );
  }
}