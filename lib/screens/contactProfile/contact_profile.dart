import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scrollable/screens/discussion/discussion_screen.dart';
import 'package:scrollable/screens/messages/message_screen.dart';
import 'package:scrollable/utils/helper.dart';
import 'dart:ui';

class ContactProfile extends StatefulWidget {
  String value;
  ContactProfile({
    this.value,
    Key key
  }) : super(key: key);

  @override
  _ContactProfileState createState() => _ContactProfileState();
}

class _ContactProfileState extends State<ContactProfile> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.value = "Agathe Queen";
  }
  @override
  Widget build(BuildContext context) {
    var e = Helper.getScreenHeight(context) / 4;
    print(e);
    return Scaffold(
      body: Container(
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        ),
        child: Column(
          children: [
            Stack(
              // overflow: Overflow.visible,
              clipBehavior: Clip.none,
              children: [
                Column(
                  children: [
                    Container(
                      height: Helper.getScreenHeight(context) / 2,
                      width: double.infinity,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        child: Image.network(
                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAsP0PcnKDBXhxu0qoT9AS9NE6PgAw-g5EIg&usqp=CAU",
                            fit: BoxFit.fill),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: Helper.getScreenHeight(context) / 2,
                      color: Colors.black.withOpacity(0.9),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomListTile(
                                    title: "Mobile",
                                    subtitle: "0395191202",
                                    icon: Ionicons.ios_call,
                                  ),
                                  CustomListTile(
                                    title: "Email",
                                    subtitle: "davelalegende5@gmail.com",
                                    icon: Icons.email,
                                  ),
                                  CustomListTile(
                                    title: "Birthday",
                                    subtitle: "Dec 29, 2000",
                                    icon: FontAwesome.birthday_cake,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Divider(),
                                  ),
                                  CustomBottomList(
                                    title: "Add to Favorites",
                                    icon: Icons.favorite_outline_rounded,
                                    widget: Container(),
                                  ),
                                  CustomBottomList(
                                    title: "Share Contact",
                                    icon: SimpleLineIcons.share_alt,
                                    widget: Container(),
                                  ),
                                  CustomBottomList(
                                    title: "Call History",
                                    icon: FontAwesome.history,
                                    widget: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      height: 15,
                                      decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Center(
                                          child: Text("18",
                                              style: TextStyle(
                                                  color: Colors.white))),
                                    ),
                                  ),
                                  Container(
                                    child: Row(children: [
                                      Icon(AntDesign.deleteuser,
                                          size: 20, color: Colors.red),
                                      SizedBox(width: 20),
                                      Text("Delete this User",
                                          style: TextStyle(color: Colors.red, fontSize: 20))
                                    ]),
                                  )
                                ]),
                          )
                        ]),
                      ),
                    )
                  ],
                ),
                Positioned(
                  bottom: Helper.getScreenHeight(context) / 2,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                      child: Container(
                        width: Helper.getScreenWidth(context),
                        height: 70,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: Text(widget.value ,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: Helper.getScreenHeight(context) / 4,
                  right: 20,
                  child: Container(
                    width: 50,
                    height: 200,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(20)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                            onPressed: () {},
                            icon:
                                Icon(Icons.date_range, color: Colors.white)),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                context, 
                                MaterialPageRoute(
                                  builder: (context) => MessageScreen()
                                ),
                              );
                            },
                            icon:
                                Icon(EvilIcons.comment, color: Colors.white)),
                        IconButton(
                            onPressed: () {},
                            icon:
                                Icon(Ionicons.ios_call, color: Colors.white)),
                        IconButton(
                            onPressed: () {},
                            icon: Icon(Feather.video, color: Colors.white)),
                      ]
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CustomBottomList extends StatelessWidget {
  const CustomBottomList({
    this.widget,
    @required this.icon,
    @required this.title,
    Key key,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final Widget widget;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(children: [
            Icon(icon, color: Colors.grey[600], size: 20),
            SizedBox(width: 20),
            Text(title, style: TextStyle(fontSize: 20, color: Colors.white)),
            SizedBox(width: 20),
            widget
          ]),
          Divider(color: Colors.transparent)
        ],
      ),
    );
  }
}

class CustomListTile extends StatelessWidget {
  const CustomListTile({
    @required this.icon,
    @required this.title,
    @required this.subtitle,
    Key key,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(
        children: [
          Icon(icon, color: Colors.grey[600], size: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(title,
                style: TextStyle(fontSize: 15, color: Colors.grey[600])),
          ),
        ],
      ),
      Padding(
        padding: const EdgeInsets.only(left: 40),
        child:
            Text(subtitle, style: TextStyle(fontSize: 20, color: Colors.white)),
      ),
      Divider(color: Colors.transparent)
    ]);
  }
}
