import 'package:flutter/material.dart';
import 'package:scrollable/components/app_bar_action.dart';
import 'package:scrollable/utils/helper.dart';

class Profile extends StatefulWidget {
  const Profile({ Key key }) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenWidth(context),
        child: Column(
          children: [
            Stack(
              overflow: Overflow.visible,
              children: [
                Container(
                  height: Helper.getScreenWidth(context) / 2,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(30),
                      bottomLeft: Radius.circular(30),
                    ),
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        Colors.black.withOpacity(0.7),
                        Colors.black.withOpacity(0.2),
                      ],
                    ),
                  ),
                  child: Image.asset("assets/profil.jpg"),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20, right: 10),
                  child: SafeArea(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Profil", style: Helper.getTheme(context).headline4),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: AppBarActions(
                                icon: Icon(Icons.settings, color: Colors.black),
                                onTap: () {},
                                ),
                              ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: AppBarActions(
                                icon: Icon(Icons.mail, color: Colors.black),
                                onTap: () {},
                              ),
                            ),
                          ]
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  left: 0,
                  bottom: -100,
                  child: Container(
                    width: double.infinity,
                    height: 150,
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 80,
                              height: 80,
                              child: CircleAvatar(
                                backgroundColor: Colors.grey[400],
                                child: ClipOval(
                                  child: SizedBox(
                                    width: 70,
                                    height: 70,
                                    child: Image.asset("assets/profil.jpg", fit: BoxFit.cover),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ElevatedButton(
                                    onPressed: () {},
                                    child: Text("18k Followers"),
                                  ),
                                  Row(
                                    children: [
                                      Text("Graphic Design"),
                                    ],
                                  )
                                ],
                              )
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.grey
                                    ),
                                    onPressed: () {},
                                    child: Text("16k Following"),
                                  ),
                                  Row(
                                    children: [
                                      IconButton(
                                        icon: Icon(Icons.edit),
                                        onPressed: () {}
                                      ),
                                      Text("Edit Profile"),
                                    ],
                                  )
                                ],
                              )
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Add bio"),
                            Text("Add to story"),
                            Text("More")
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ]
        ),
      ),
    );
  }
}