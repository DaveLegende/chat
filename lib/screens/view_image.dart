import 'package:flutter/material.dart';
import 'package:scrollable/utils/helper.dart';

class ViewImage extends StatefulWidget {
  final int index;
  const ViewImage({this.index});

  @override
  _ViewImageState createState() => _ViewImageState();
}

class _ViewImageState extends State<ViewImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU",
          fit: BoxFit.fill
        ),
      ),
    );
  }
}