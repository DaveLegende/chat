import 'package:camera/camera.dart';
import 'package:flutter/material.dart';


List<CameraDescription> cameras;

class CameraScreen extends StatefulWidget {
  const CameraScreen({ Key key }) : super(key: key);

  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  CameraController _cameraController;

  Future<void> cameraValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _cameraController = CameraController(cameras[0], ResolutionPreset.high);
    cameraValue = _cameraController.initialize();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          CameraPreview(_cameraController)
        ]
      )
    );
  }
}