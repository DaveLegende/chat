// import 'package:emoji_picker/emoji_picker.dart';
import 'dart:io';

import 'package:emoji_picker/emoji_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scrollable/screens/camera/camera_screen.dart';
import 'package:scrollable/utils/constants.dart';
import 'package:scrollable/utils/helper.dart';
import 'package:image_picker/image_picker.dart';

class CustomTextField extends StatefulWidget {
  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool show = false;
  FocusNode focusNode = FocusNode();
  String _typeText = "";
  TextEditingController _controller = TextEditingController();

  
  File _image_id;
  File _image_vic;

  
  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      if(focusNode.hasFocus) {
        setState(() {
          show = false;
        });
      }
    });
    _controller = TextEditingController();
    _controller.addListener(() {
      setState(() {
        _typeText = _controller.text;
      });
    });

  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    child: TextField(
                      focusNode: focusNode,
                      cursorColor: mbSeconderedColorCpfind,
                      cursorWidth: 1,
                      controller: _controller,
                      decoration: InputDecoration(
                        hoverColor: Colors.black38,
                        prefixIcon: IconButton(
                          icon: Icon(
                            Icons.sentiment_very_satisfied,
                            color: Colors.blue,
                          ),
                          onPressed: () {
                            focusNode.unfocus();
                            focusNode.canRequestFocus = false;
                            setState(() {
                              show = !show;
                            });
                          },
                        ),
                        // ? IconButton(
                        //     icon: Icon(
                        //       AntDesign.message1,
                        //       color: mbSeconderedColorCpfind,
                        //     ),
                        //     onPressed: () {},
                        //   )
                        suffixIcon: _controller.text.isEmpty
                            ? 
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  onPressed: () {
                                    showModalBottomSheet(
                                      backgroundColor: Colors.transparent,
                                      context: context,
                                      builder: (builder) => bottomDialog()
                                    );
                                  },
                                  icon: Icon(Entypo.attachment, size: 20, color: Colors.blue)
                                ),
                                IconButton(
                                  onPressed: () {
                                    // navigate(context, CameraScreen());
                                    _openIdentiteCamera(context);
                                  },
                                  icon: Icon(Feather.camera, size: 20, color: Colors.blue)
                                )
                              ],
                            )
                            : IconButton(
                                icon: Icon(
                                  Icons.send,
                                  color: mbSeconderedColorCpfind,
                                ),
                                onPressed: () {},
                              ),
                        // prefixIcon: IconButton(
                        //     icon: _controller.text.isEmpty ? Icon(AntDesign.message1, color: mbSeconderedColorCpfind,) : Icon(Icons.send, color: mbSeconderedColorCpfind,),
                        //     onPressed: () {}),
                        hintText: "Taper message...",
                        hintStyle: TextStyle(color: Colors.black38),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.circular(25),
                          borderSide: BorderSide(color: Colors.grey[400]),
                        ),
                      ),
                    ),
                  )
                )
              ),
              microphoneVocal(),
            ],
          ),
          show ? _emojiSelected() : Container()
        ],
      ),
    );
    
  }

  Expanded microphoneVocal() {
    var isRecording = false;
    return Expanded(
      flex: 1,
      child: CircleAvatar(
        backgroundColor: Colors.blue,
        child: IconButton(
          icon: Icon(isRecording ? FontAwesome.microphone_slash : MaterialCommunityIcons.microphone),
          iconSize: 20,
          color: isRecording ? Colors.red : Colors.white,
          onPressed: () {
            setState(() {
              isRecording = !isRecording;
          });
      },
        ),
      ),
    );
  }

  navigate(BuildContext context, Object object) {
    return Navigator.push(context,
      MaterialPageRoute(
        builder: (context) => object
      )
    );
  }

  _emojiSelected() {
    return EmojiPicker(
      rows: 4,
      columns: 7,
      onEmojiSelected: (emoji, category) {
        print(emoji);
        setState(() {
          _controller.text += emoji.emoji;
        });
      }
    );
  }

  Widget bottomDialog() {
    return Container(
      height: Helper.getScreenHeight(context) / 2.5,
      width: Helper.getScreenWidth(context),
      child: Card(
        margin: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IndividualIconItem(
                  icon: Icons.insert_drive_file,
                  text: "Documents",
                  color: Colors.blue.withOpacity(0.2),
                  onClick: () {}
                ),
                IndividualIconItem(
                  icon: Feather.camera,
                  text: "Caméra",
                  color: Colors.red,
                  onClick: () {
                    navigate(context, CameraScreen());
                  }
                ),
                IndividualIconItem(
                  icon: Icons.photo,
                  text: "Galerie",
                  color: Colors.purpleAccent,
                  onClick: () {
                    setState(() {
                      _openIdentiteGalerie(context);
                    });
                  }
                )
              ]
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IndividualIconItem(
                  icon: Icons.headset,
                  text: "Audio",
                  color: Colors.orange,
                  onClick: () {}
                ),
                IndividualIconItem(
                  icon: Icons.location_pin,
                  text: "Localisation",
                  color: Colors.green,
                  onClick: () {}
                ),
                IndividualIconItem(
                  icon: Icons.person,
                  text: "Contact",
                  color: Colors.blue,
                  onClick: () {}
                )
              ]
            )
          ],
        ),
      )
    );
  }

  _openIdentiteGalerie(BuildContext context) async {
    // ignore: invalid_use_of_visible_for_testing_member
    var picture = await ImagePicker.platform.pickImage(source: ImageSource.gallery);

    this.setState(() {
      _image_id = picture as File;
    });
    Navigator.of(context).pop();
  }

  _openIdentiteCamera(BuildContext context) async {
    // ignore: invalid_use_of_visible_for_testing_member
    var picture = await ImagePicker.platform.pickImage(source: ImageSource.camera);

    this.setState(() {
      _image_id = picture as File;
    });
    Navigator.of(context).pop();
  }


  Future<Null> getImageIdentiteByCamera() async {
    // ignore: invalid_use_of_visible_for_testing_member
    final image = await ImagePicker.platform.pickImage(source: ImageSource.camera);

    this.setState(() {
      _image_id = image as File;
    });
  }
}


class IndividualIconItem extends StatelessWidget {
  const IndividualIconItem({
    @required this.icon,
    @required this.color,
    @required this.text,
    @required this.onClick,
    Key key,
  }) : super(key: key);

  final IconData icon;
  final Color color;
  final String text;
  final Function onClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Column(
        children: [
          CircleAvatar(
            backgroundColor: color,
            radius: 30,
            child: Icon(
              icon,
              color: Colors.white,
              size: 20
            )
          ),
          Text(text)
        ]
      ),
    );
  }
}

// class EmojiSelected extends StatefulWidget {
//   const EmojiSelected({
//     Key key,
//   }) : super(key: key);

//   @override
//   _EmojiSelectedState createState() => _EmojiSelectedState();
// }

// class _EmojiSelectedState extends State<EmojiSelected> {
//   @override
//   Widget build(BuildContext context) {
//     return EmojiPicker(
//       rows: 4,
//       columns: 7,
//       onEmojiSelected: (emoji, category) {
//         print(emoji);
//         setState(() {
//           _controller.text += emoji.emoji;
//         });
//       }
//     );
//   }
// }
