import 'package:flutter/material.dart';
import 'package:scrollable/components/publish_stories.dart';
import '../accueil.dart';
import 'components/user_comment.dart';

class AllComment extends StatefulWidget {
  final int index;
  const AllComment({ 
    this.index,
    Key key 
  }) : super(key: key);

  @override
  _AllCommentState createState() => _AllCommentState();
}

class _AllCommentState extends State<AllComment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PublishStories(index: widget.index),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Divider(),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: 10,
                    shrinkWrap: true,
                    itemBuilder: (context, index) => UserComment()
                  )
                )
            ]
          ),
        ),
      ),
    );
  }
}