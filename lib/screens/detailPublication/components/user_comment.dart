import 'package:flutter/material.dart';
import 'package:scrollable/utils/helper.dart';

class UserComment extends StatelessWidget {
  const UserComment({ Key key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 15,
                backgroundImage: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
              ),
              SizedBox(width: 10),
              Align(
                alignment: Alignment.topCenter,
                child: Text("Eric Davidson", style: TextStyle(fontSize: 20)),
              )
            ]
          ),
          Padding(
            padding: EdgeInsets.only(left: 40, top: 5),
            child: Text(
              "Many desktop publishing packages and web pages editors now use",
              maxLines: 2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Row(
              children: [
                Text("13 mins ago . ", style: TextStyle(color: Colors.grey[400])),
                TextButton(
                  child: Text("Reply", style: TextStyle(color: Colors.grey[400])),
                  onPressed: () {}
                )
              ]
            ),
          ),
        //   ListTile(
        //   leading: CircleAvatar(
        //     radius: 20,
        //     backgroundImage: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
        //   ),
        //   title: Text("Eric Davidson"),
        //   subtitle: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Text(
        //         "Many desktop publishing packages and web pages editors now use",
        //         maxLines: 2,
        //       ),
        //       Text("13 mins ago . Reply"),
        //     ],
        //   )
        // ),
        Divider(color: Colors.transparent)
        ]
      )
    );
  }
}