import 'package:flutter/material.dart';
import 'package:scrollable/components/publish_stories.dart';
import 'package:scrollable/utils/helper.dart';
import 'package:permission_handler/permission_handler.dart';
import 'contact.dart';

class Accueil extends StatefulWidget {
  const Accueil({ Key key }) : super(key: key);

  @override
  _AccueilState createState() => _AccueilState();
}

class _AccueilState extends State<Accueil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text("Stories", style: Helper.getTheme(context).headline3),
              ),
              Container(
                height: 60,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: CircleAvatar(
                            radius: 30,
                            backgroundImage: NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU")
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: CircleAvatar(
                            radius: 10,
                            child: Icon(Icons.add, size: 15),
                          ),
                        )
                      ],
                    ),
                    StoryAvatar(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                    StoryAvatar(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                    StoryAvatar(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                    StoryAvatar(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                    StoryAvatar(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                    StoryAvatar(url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcPI1asYofkbUwv9E7WYb92FImgseG7YNbxA&usqp=CAU"),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: Divider(),
              ),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: 10,
                itemBuilder: (context, index) => PublishStories(index: index),
              )
            ]
          )
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        onPressed: () async {
          final PermissionStatus permissionStatus = await _getPermission();
          if (permissionStatus == PermissionStatus.granted) {
            //We can now access our contacts here
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => ContactScreen())
            );
          }
        },
        // onPressed: () {
        //   Navigator.push(context, MaterialPageRoute(
        //     builder: (context) => ContactScreen())
        //   );
        // },
        child: Icon(Icons.add, color: Colors.white, size: 15),
      ),
    );
  }

  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
      await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.restricted;
    } else {
      return permission;
    }
  }
}


class StoryAvatar extends StatelessWidget {
  const StoryAvatar({
    @required String url,
    Key key,
  }) : _url = url, super(key: key);

  final String _url;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10),
      height: 60,
      width: 60,
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Colors.red,
      ),
      child: CircleAvatar(
        radius: 18,
        backgroundImage: NetworkImage(_url),
      ),
    );
  }
}




// class SeeContactsButton extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return RaisedButton(
//       onPressed: () async {
//         final PermissionStatus permissionStatus = await _getPermission();
//         if (permissionStatus == PermissionStatus.granted) {
//           //We can now access our contacts here
//           Navigator.push(context, MaterialPageRoute(
//             builder: (context) => ContactsPage())
//           );
//         }
//       },
//       child: Container(child: Text('See Contacts')),
//     );
//   }

//   //Check contacts permission
//   Future<PermissionStatus> _getPermission() async {
//     final PermissionStatus permission = await Permission.contacts.status;
//     if (permission != PermissionStatus.granted &&
//         permission != PermissionStatus.denied) {
//       final Map<Permission, PermissionStatus> permissionStatus =
//       await [Permission.contacts].request();
//       return permissionStatus[Permission.contacts] ??
//           PermissionStatus.restricted;
//     } else {
//       return permission;
//     }
//   }
// }