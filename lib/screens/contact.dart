import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:scrollable/components/search_bar.dart';
import 'package:scrollable/utils/constants.dart';

import 'contactProfile/contact_profile.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({ Key key }) : super(key: key);

  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  List<Contact> contacts = [];

  @override
  void initState() {
    super.initState();
    getAllContacts();
  }

  getAllContacts() async {
    List<Contact> _contacts = (await ContactsService.getContacts()).toList();
    setState(() {
      contacts = _contacts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: appBarColor,
        title: Text("Contact Book"),
        leading: Icon(Icons.arrow_back_ios, color: Colors.white),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 50),
            child: IconButton(
              onPressed: () {},
              icon: Icon(Icons.add, color: Colors.blue)
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            SearchBarComponent(
              hintTextColor: Colors.black,
              prefixIconColor: Colors.black
            ),
            Expanded(
              child: Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: contacts.length,
                  itemBuilder: (context,index){
                    Contact contact = contacts[index];
                    return Column(
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ContactProfile(value: "value"))
                            );
                          },
                          child: ListTile(
                            title: Text(contact.displayName),
                            subtitle: Text(
                              contact.phones.elementAt(0).value != null ? 
                              contact.phones.elementAt(0).value : 
                              'Default Text'
                            ),
                            leading: (contact.avatar != null && contact.avatar.length > 0) ?
                              CircleAvatar(
                                backgroundImage: MemoryImage(contact.avatar),
                              ) : 
                              CircleAvatar(
                                child: Text(contact.initials()),
                              )
                          ),
                        ),
                        Divider(color: Colors.white.withOpacity(0.0)),
                      ],
                    );
                  }
                )
              ),
            )
          ],
        )
      ),
    );
  }

  //Check contacts permission
  
}


// import 'package:flutter/material.dart';
// import 'package:contacts_service/contacts_service.dart';

// class ContactsPage extends StatefulWidget {
//   @override
//   _ContactsPageState createState() => _ContactsPageState();
// }

// class _ContactsPageState extends State<ContactsPage> {
//   Iterable<Contact> _contacts;

//   @override
//   void initState() {
//     getContacts();
//     super.initState();
//   }

//   Future<void> getContacts() async {
//     //Make sure we already have permissions for contacts when we get to this
//     //page, so we can just retrieve it
//     final Iterable<Contact> contacts = await ContactsService.getContacts();
//     setState(() {
//       _contacts = contacts;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: (Text('Contacts')),
//       ),
//       body: _contacts != null
//           //Build a list view of all contacts, displaying their avatar and
//           // display name
//           ? ListView.builder(
//               itemCount: _contacts?.length ?? 0,
//               itemBuilder: (BuildContext context, int index) {
//                 Contact contact = _contacts?.elementAt(index);
//                 return ListTile(
//                   contentPadding:
//                       const EdgeInsets.symmetric(vertical: 2, horizontal: 18),
//                   leading: (contact.avatar != null && contact.avatar.isNotEmpty)
//                       ? CircleAvatar(
//                           backgroundImage: MemoryImage(contact.avatar),
//                         )
//                       : CircleAvatar(
//                           child: Text(contact.initials()),
//                           backgroundColor: Theme.of(context).accentColor,
//                         ),
//                   title: Text(contact.displayName ?? ''),
//                   //This can be further expanded to showing contacts detail
//                   // onPressed().
//                 );
//               },
//             )
//           : Center(child: const CircularProgressIndicator()),
//     );
//   }
// }