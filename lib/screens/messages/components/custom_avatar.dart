import 'package:flutter/material.dart';

class CustomAvatar extends StatelessWidget {
  const CustomAvatar({
    @required this.radiusImage,
    @required this.radiusOnLine,
    @required this.rightSpace,
    @required this.statusImageUrl,
    Key key,
  }) : super(key: key);

  final double radiusImage;
  final double radiusOnLine;
  final double rightSpace;
  final String statusImageUrl;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Stack(
        children: [
          CircleAvatar(
            radius: radiusImage,
            backgroundImage: NetworkImage(statusImageUrl),
          ),
          Positioned(
            bottom: 0,
            right: rightSpace,
            child: CircleAvatar(
              radius: radiusOnLine,
              backgroundColor: Colors.green,
            ),
          )
        ],
      ),
    );
  }
}
