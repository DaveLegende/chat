import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scrollable/components/search_bar.dart';
import 'package:scrollable/models/message_model.dart';
import 'package:scrollable/screens/discussion/discussion_screen.dart';
import 'package:scrollable/utils/helper.dart';

import '../pp_view.dart';
import 'components/custom_avatar.dart';

class MessageScreen extends StatefulWidget {
  // MessageScreen({
  //   this.url
  // });
  // String url;

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {

  @override
  void initState() { 
    super.initState();
    // idget.url = 'https://maviaa.files.wordpress.com/2012/12/stefan-salvatore-mobile-wallpaper.jpg';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
        title: Text(
          'Messages',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(SimpleLineIcons.share_alt, color: Colors.blue, size: 20),
          ),
          IconButton(
            icon: Icon(Icons.group, color: Colors.blue),
            onPressed: () {},
          ),
        ],
      ),
      body: Container(
        color: Colors.black,
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        child: Column(
          children: [
            SearchBarComponent(
              hintTextColor: Colors.white,
              prefixIconColor: Colors.white
            ),
            StatusPost(),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Divider(color: Colors.grey[500]),
            ),
            Container(
              child: Expanded(
                flex: 5,
                child: Container(
                  margin: const EdgeInsets.only(left: 5, top: 20),
                  child: ListView.builder(
                    itemCount: chats.length,
                    itemBuilder: (context, index) => InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChatScreen(user: chats[index].sender)
                          )
                        );
                      },
                      child: Container(
                        // padding: const EdgeInsets.symmetric(vertical: 10),
                        child: ListTile(
                          title: Text(
                            chats[index].sender.name, 
                            style: TextStyle(color: Colors.white, fontSize: 20, fontStyle: FontStyle.italic)
                          ),
                          subtitle: Text(
                            chats[index].text, 
                            style: TextStyle(color: Colors.white),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          leading: InkWell(
                            child: Stack(
                              children: [
                                InkWell(
                                  onTap: () {
                                    Navigator.push(context, 
                                      MaterialPageRoute(builder: (context) => PPView())
                                    );
                                  },
                                  child: Hero(
                                    tag: 'tag',
                                    child: CircleAvatar(
                                      radius: 30,
                                      backgroundImage: NetworkImage(chats[index].sender.imageUrl),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 10,
                                  child: chats[index].sender.isOnline ? CircleAvatar(
                                    radius: 5,
                                    backgroundColor: Colors.green,
                                  ) : Container(),
                                )
                              ],
                            ),
                          ),
                          // leading: CustomAvatar(
                          //   statusImageUrl: chats[index].sender.imageUrl,
                          //   radiusImage: 30,
                          //   radiusOnLine: 5,
                          //   rightSpace: 10,
                          // ),
                          trailing: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(chats[index].time, style: TextStyle(color: Colors.white)),
                              SizedBox(height: 5),
                              Container(
                                width: 30,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5),
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Center(
                                  child: Text("+2",style: TextStyle(color: Colors.white))
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ),
                ),
              ),
            )
          ]
        ),
      ),
      // body: ListView.builder(
      //   itemCount: chats.length,
      //   itemBuilder: (BuildContext context, int index) {
      //     final Message chat = chats[index];
      //     return GestureDetector(
      //       // onTap: () => Navigator.push(
      //       //   context,
      //       //   MaterialPageRoute(
      //       //     builder: (_) => ChatScreen(
      //       //       user: chat.sender,
      //       //     ),
      //       //   ),
      //       // ),
      //       // child: Container(
      //       //   child: ListTile(
      //       //     leading: Stack(
      //       //       children: [
      //       //         Padding(
      //       //           padding: const EdgeInsets.only(left: 10),
      //       //           child: CircleAvatar(
      //       //             radius: 30,
      //       //             backgroundImage: NetworkImage(chat.sender.imageUrl)
      //       //           ),
      //       //         ),
      //       //         chat.sender.isOnline ? 
      //       //         Positioned(
      //       //           bottom: 0,
      //       //           right: 10,
      //       //           child: CircleAvatar(
      //       //             backgroundColor: Colors.green,
      //       //             radius: 5,
      //       //           ),
      //       //         ) :
      //       //         Container(),
      //       //       ],
      //       //     ),
      //       //     title: Text(
      //       //       chat.sender.name,
      //       //       style: TextStyle(
      //       //         fontSize: 16,
      //       //         fontWeight: FontWeight.bold,
      //       //       ),
      //       //     ),
      //       //     subtitle: Text(
      //       //       chat.text,
      //       //       style: TextStyle(
      //       //         fontSize: 13,
      //       //         color: Colors.black54,
      //       //       ),
      //       //       overflow: TextOverflow.ellipsis,
      //       //       maxLines: 2,
      //       //     ),
      //       //     trailing: Text(
      //       //       chat.time,
      //       //       style: TextStyle(
      //       //         fontSize: 11,
      //       //         fontWeight: FontWeight.w300,
      //       //         color: Colors.black54,
      //       //       ),
      //       //     ),
      //       //   ),
      //       // ),
      //       child: Container(
      //         padding: EdgeInsets.only(
      //           left: 10,
      //           top: 15,
      //         ),
      //         child: Row(
      //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //           children: <Widget>[
      //             Row(
      //               children: [
      //                 Stack(
      //                   children: [
      //                     Padding(
      //                       padding: const EdgeInsets.only(left: 10),
      //                       child: CircleAvatar(
      //                         radius: 30,
      //                         backgroundImage: NetworkImage(chat.sender.imageUrl)
      //                       ),
      //                     ),
      //                     chat.sender.isOnline ? 
      //                     Positioned(
      //                       bottom: 0,
      //                       right: 0,
      //                       child: CircleAvatar(
      //                         backgroundColor: Colors.green,
      //                         radius: 10,
      //                       ),
      //                     ) : 
      //                     Container(),
      //                   ],
      //                 ),
      //                 Container(
      //                   padding: EdgeInsets.only(
      //                     left: 20,
      //                   ),
      //                   child: Column(
      //                     crossAxisAlignment: CrossAxisAlignment.start,
      //                     children: <Widget>[
      //                       Text(
      //                         chat.sender.name,
      //                         style: TextStyle(
      //                           fontSize: 16,
      //                           fontWeight: FontWeight.bold,
      //                         ),
      //                       ),
      //                       SizedBox(
      //                         height: 10,
      //                       ),
      //                       Container(
      //                         alignment: Alignment.topLeft,
      //                         child: Text(
      //                           chat.text,
      //                           style: TextStyle(
      //                             fontSize: 13,
      //                             color: Colors.black54,
      //                           ),
      //                           overflow: TextOverflow.ellipsis,
      //                           maxLines: 2,
      //                         ),
      //                       ),
      //                     ],
      //                   ),
      //                 ),
      //               ],
      //             ),
      //             Expanded(
      //               child: Container(
      //                 child: Text(
      //                   chat.time,
      //                   style: TextStyle(
      //                     fontSize: 11,
      //                     fontWeight: FontWeight.w300,
      //                     color: Colors.black54,
      //                   ),
      //                 ),
      //               ),
      //             ),
      //             // Container(
      //             //   width: MediaQuery.of(context).size.width * 0.65,
      //             //   padding: EdgeInsets.only(
      //             //     left: 20,
      //             //   ),
      //             //   child: Column(
      //             //     children: <Widget>[
      //             //       Row(
      //             //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //             //         children: <Widget>[
      //             //           Text(
      //             //             chat.sender.name,
      //             //             style: TextStyle(
      //             //               fontSize: 16,
      //             //               fontWeight: FontWeight.bold,
      //             //             ),
      //             //           ),
      //             //           Text(
      //             //             chat.time,
      //             //             style: TextStyle(
      //             //               fontSize: 11,
      //             //               fontWeight: FontWeight.w300,
      //             //               color: Colors.black54,
      //             //             ),
      //             //           ),
      //             //         ],
      //             //       ),
      //             //       SizedBox(
      //             //         height: 10,
      //             //       ),
      //             //       Container(
      //             //         alignment: Alignment.topLeft,
      //             //         child: Text(
      //             //           chat.text,
      //             //           style: TextStyle(
      //             //             fontSize: 13,
      //             //             color: Colors.black54,
      //             //           ),
      //             //           overflow: TextOverflow.ellipsis,
      //             //           maxLines: 2,
      //             //         ),
      //             //       ),
      //             //     ],
      //             //   ),
      //             // ),
      //           ],
      //         ),
      //       ),
      //     );
      //   },
      // ),
    );
  }
}

class StatusPost extends StatelessWidget {
  const StatusPost({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        flex: 1,
        child: Container(
          margin: EdgeInsets.only(left: 20),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 20,
            itemBuilder: (context, index) => Container(
              padding: const EdgeInsets.only(right: 20),
              child: Column(
                children: [
                  CustomAvatar(
                    statusImageUrl: "https://static9.depositphotos.com/1687987/1171/i/600/depositphotos_11714897-stock-photo-girl-with-bouquet.jpg",
                    radiusImage: 25,
                    radiusOnLine: 5,
                    rightSpace: 5
                  ),
                  SizedBox(height: 5),
                  Text("Queen", style: TextStyle(color: Colors.white))
                ],
              ),
            ),
          ),
        ),
      )
    );
  }
}
