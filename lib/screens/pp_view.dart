import 'package:flutter/material.dart';
import 'package:scrollable/utils/helper.dart';

class PPView extends StatefulWidget {
  const PPView({ 
    Key key 
  }) : super(key: key);

  @override
  _PPViewState createState() => _PPViewState();
}

class _PPViewState extends State<PPView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        child: Hero(
          tag: 'tag',
          child: ClipRRect(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
            child: Image.network(""),
          ),
        ),
      ),
    );
  }
}