class User {
  final int id;
  final String name;
  final String imageUrl;
  final bool isOnline;

  User({
    this.id,
    this.name,
    this.imageUrl,
    this.isOnline,
  });
}

// YOU - current user
final User currentUser = User(
  id: 0,
  name: 'Dave',
  imageUrl: 'https://maviaa.files.wordpress.com/2012/12/stefan-salvatore-mobile-wallpaper.jpg',
  isOnline: true,
);

// USERS
final User mathieu = User(
  id: 1,
  name: 'Mathieu',
  imageUrl: 'https://c8.alamy.com/compfr/2bwwk4p/jeune-femme-souriante-dehors-dans-la-ville-cadrage-avec-les-mains-une-fille-souriant-et-doothy-une-expression-positive-du-visage-d-emotion-humaine-faisant-un-carre-avec-des-mains-looki-2bwwk4p.jpg',
  isOnline: true,
);
final User agathe = User(
  id: 2,
  name: 'Agathe',
  imageUrl: 'https://previews.123rf.com/images/kho/kho1303/kho130300231/18628871-no%C3%ABl-fille-belle-jeune-femme-souriante-sur-fond-bleu.jpg',
  isOnline: true,
);
final User holali = User(
  id: 3,
  name: 'Holali',
  imageUrl: 'https://previews.123rf.com/images/johanjk/johanjk1406/johanjk140600047/29174481-gros-plan-portrait-de-jeune-femme-souriante-fille-flirter-noir-et-blanc-non-isol%C3%A9.jpg',
  isOnline: false,
);
final User august = User(
  id: 4,
  name: 'August Fx',
  imageUrl: 'https://thumbs.dreamstime.com/z/jolie-jeune-femme-souriante-et-qui-montre-les-deux-mains-montrant-ouvertes-paumes-la-pr%C3%A9sentation-publicit%C3%A9-quelque-chose-160348026.jpg',
  isOnline: false,
);
final User sylvain = User(
  id: 5,
  name: 'Sylvain',
  imageUrl: 'https://image.freepik.com/photos-gratuite/belle-jeune-femme-souriante-recherche-fille-branchee-dans-vetements-ete-decontractes_158538-1496.jpg',
  isOnline: true,
);
final User armand = User(
  id: 6,
  name: 'Armand',
  imageUrl: 'https://static.vecteezy.com/ti/photos-gratuite/p1/876627-portrait-d-ete-d-une-belle-femme-souriante-dans-un-parc-photo.jpg',
  isOnline: false,
);
final User henoc = User(
  id: 7,
  name: 'Hénoc',
  imageUrl: 'https://previews.123rf.com/images/rido/rido1701/rido170100017/71438841-gros-plan-d-une-jeune-femme-souriante-regardant-la-cam%C3%A9ra-portrait-heureux-fille-brune-souriante-%C3%A0-l.jpg',
  isOnline: false,
);
final User cephas = User(
  id: 8,
  name: 'Céphas',
  imageUrl: 'https://st3.depositphotos.com/12039320/16733/i/600/depositphotos_167334326-stock-photo-beautiful-woman-in-straw-hat.jpg',
  isOnline: false,
);