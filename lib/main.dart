import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scrollable/utils/helper.dart';
import 'components/app_bar_action.dart';
import 'nouvellePage.dart';
import 'screens/accueil.dart';
import 'screens/camera/camera_screen.dart';

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: TextTheme(
          headline4: TextStyle(
            color: Colors.white,
            // fontWeight: FontWeight.bold
          ),
          headline6: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold
          )
        ),
        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      // home: MyHomePage(title: 'Scrollable'),
      home: Accueil(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Activite> mesActivites = [
    new Activite("Vélo", Icons.directions_bike),
    new Activite("Peinture", Icons.palette),
    new Activite("Golf", Icons.golf_course),
    new Activite("Arcade", Icons.gamepad),
    new Activite("Bricolage", Icons.build),
    new Activite("Vélo", Icons.directions_bike),
    new Activite("Peinture", Icons.palette),
    new Activite("Golf", Icons.golf_course),
    new Activite("Arcade", Icons.gamepad),
    new Activite("Bricolage", Icons.build),
    new Activite("Vélo", Icons.directions_bike),
    new Activite("Peinture", Icons.palette),
    new Activite("Golf", Icons.golf_course),
    new Activite("Arcade", Icons.gamepad),
    new Activite("Bricolage", Icons.build),
  ];

  @override
  Widget build(BuildContext context) {

    Orientation orientation = MediaQuery.of(context).orientation;
    print(orientation);
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.teal,
      //   title: Text(widget.title),
      // ),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(Helper.getScreenHeight(context) / 3),
        child: AppBar(
          backgroundColor: Colors.white,
          title: Text("Profil", style: Helper.getTheme(context).headline3),
          actions: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: AppBarActions(
                icon: Icon(Icons.settings, color: Colors.black),
                onTap: () {},
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: AppBarActions(
                icon: Icon(Icons.mail, color: Colors.black),
                onTap: () {},
              ),
            ),
          ],
          flexibleSpace: Stack(
            overflow: Overflow.visible,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/profil.jpg"),
                      fit: BoxFit.fill
                    ),
                  ),
                ),
              ),
              Positioned(
                right: 0,
                left: 0,
                bottom: -35,
                child: Container(
                  height: 80,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CircleAvatar(
                        radius: 80,
                        backgroundColor: Colors.grey[400],
                        child: ClipOval(
                          child: SizedBox(
                            width: 70,
                            height: 70,
                            child: Image.asset("assets/profil.jpg", fit: BoxFit.cover),
                          ),
                        ),
                      ),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ElevatedButton(
                              onPressed: () {},
                              child: Text("18k Followers"),
                            ),
                            Text("Graphic Design/Product Designer",
                              maxLines: 2,
                            )
                          ],
                        )
                      ),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ElevatedButton(
                              onPressed: () {},
                              child: Text("16k Following"),
                            ),
                            Row(
                              children: [
                                IconButton(
                                  icon: Icon(Icons.edit),
                                  onPressed: () {}
                                ),
                                Text("Edit Profile"),
                              ],
                            )
                          ],
                        )
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ), 
      ),
      body: Center(
          child: (orientation == Orientation.portrait) ? liste() : grille(),
      ),
    );
  }

  Widget grille() {
    return new GridView.builder(
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4, crossAxisSpacing: 3, childAspectRatio: 2.0),
      itemCount: mesActivites.length,
      itemBuilder: (context, i) {
        return new Container(
          margin: EdgeInsets.all(2.5),
          child: new Card(
            elevation: 10.0,
            child: new InkWell(
              onTap: () {
                setState(() {
                  Navigator.push(
                    context, new MaterialPageRoute(
                      builder: (BuildContext context) {
                        return new NouvellePage();
                      }
                  ),
                  );
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  new Text("Activité:", style: new TextStyle(color: Colors.teal, fontSize: 15.0),),
                  new Icon(mesActivites[i].icone, color: Colors.teal, size: 40.0,),
                  new Text(mesActivites[i].nom, style: new TextStyle(color: Colors.teal, fontSize: 25.0, fontStyle: FontStyle.italic),)
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget liste() {
    return ListView.builder(
        itemCount: mesActivites.length,
        itemBuilder: (context, i) {
          Activite activite = mesActivites[i];
          String key = mesActivites[i].nom;
          return new Dismissible(
            key: new Key(key),
            child: new Container(
              padding: EdgeInsets.all(2.5),
              height: 120.5,
              child: new Card(
                elevation: 7.5,
                child: new InkWell(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                            return new NouvellePage();
                          }
                      ),
                      );
                    });
                  },
                  onLongPress: (() => print("appuie longue")),
                  child: new Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        new Icon(activite.icone, size: 75.0, color: Colors.teal,),
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            new Text("Activité:", style: new TextStyle(color: Colors.teal, fontSize: 20.0),),
                            new Text(activite.nom, style: new TextStyle(color: Colors.teal[700], fontSize: 30.0),),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            background: new Container(
              padding: EdgeInsets.only(right: 20.0),
              color: Colors.blueGrey,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Text('Supprimer', style: new TextStyle(color: Colors.white),),
                  new Icon(Icons.delete, color: Colors.white),
                ],
              ),
            ),
            onDismissed: (direction) {
              setState(() {
                print(mesActivites[i].nom);
                mesActivites.removeAt(i);
              });
            },
          );
        }
    );
  }
}


class Activite {
  String nom;
  IconData icone;

  Activite(String nom, IconData icone) {
    this.nom = nom;
    this.icone = icone;
  }
}
