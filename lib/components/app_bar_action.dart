import 'package:flutter/material.dart';

class AppBarActions extends StatelessWidget {
  const AppBarActions({
    @required Widget icon,
    @required Function onTap,
    Key key,
  }) : _icon = icon, _onTap = onTap, super(key: key);

  final Widget _icon;
  final Function _onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 40,
      decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: IconButton(
        onPressed: _onTap, 
        icon: _icon
      ),
    );
  }
}