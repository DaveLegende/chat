import 'package:flutter/material.dart';
import 'package:scrollable/utils/helper.dart';

class SearchBarComponent extends StatelessWidget {
  const SearchBarComponent({
    @required this.hintTextColor,
    @required this.prefixIconColor,
    Key key,
  }) : super(key: key);

  final Color hintTextColor;
  final Color prefixIconColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Helper.getScreenWidth(context),
      height: 50,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
        color: Colors.grey[200].withOpacity(0.2),
        borderRadius: BorderRadius.circular(30),
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: "Search...",
          hintStyle: TextStyle(
            color: hintTextColor,
          ),
          prefixIcon: Icon(Icons.search, color: prefixIconColor),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
      ),
    );
  }
}
