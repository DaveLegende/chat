import 'package:flutter/material.dart';
import 'package:scrollable/screens/detailPublication/all_comment.dart';
import 'package:scrollable/screens/profile_stories.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:scrollable/utils/helper.dart';

class PublishStories extends StatefulWidget {
  final int index;
  const PublishStories({
    this.index,
    Key key,
  }) : super(key: key);

  @override
  _PublishStoriesState createState() => _PublishStoriesState();
}

class _PublishStoriesState extends State<PublishStories> {

  List<String> url = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrV13N2u8gZdYt8yJVur7J21H2sKihyVm3Zw&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTecoInAkROPa_xVGPOPeMPZeW5sDKLloMbNw&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVoFK2UYeQX5TPA5lvMAeHwiz01MvXIkA_dQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4mZWAZ6H7rMwse_xz7xZCX6a4blTjTD_eSQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpIARy7jSiGEZIOnN-d0dKW8XM6mMVvu5NzA&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTP9GcR5bgzx6myQSVwmtSY5vo1zaQV0Ujn8A&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGPKeFtGMglRntR3_kael8YLCz_XVX8WZTeg&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwrfV-b48py9BldeYT_s6sZCKiBgIcyCb-1g&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVFqOBf6V8teqFG6g0M1Ll97cMfo0JCrfy9g&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQu1aOSRHbO8QFAx6VNi0qX49WroV7tVs3i8g&usqp=CAU"
  ];

  @override
  Widget build(BuildContext context) {
    bool isfavorite = false;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Stack(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context) => ProfileStories())
                          );
                        },
                        child: CircleAvatar(
                          radius: 20,
                          backgroundImage: NetworkImage("${url[widget.index]}"),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: CircleAvatar(
                          radius: 5,
                          backgroundColor: Colors.green,
                        ),
                      )
                    ],
                  ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Marcus Sanchez", style: TextStyle(fontSize: 20)),
                    Text("15 min ago")
                  ],
                )
                ],
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Feather.more_horizontal),
              )
            ],
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            height: Helper.getScreenWidth(context) * 0.7,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  spreadRadius: 2,
                  blurRadius: 20,
                  offset: Offset(0, 10)
                )
              ],
              // image: DecorationImage(
              //   fit: BoxFit.cover,
              //   image: NetworkImage("${url[widget.index]}")
              // )
            ),
            child: InkWell(
              onTap: () {
                Navigator.push(context,
                  MaterialPageRoute(
                    builder: (context) => AllComment(index: widget.index)
                  )
                );
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  "${url[widget.index]}",
                  fit: BoxFit.cover
                ),
              )
            ),
          ),
          SizedBox(height: 10),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isfavorite = !isfavorite;
                            });
                          },
                          icon: Icon(Icons.favorite,
                            color: isfavorite == true ? Colors.red : Colors.black,
                          )
                        ),
                        // SizedBox(width: 10),
                        Text("128")
                      ],
                    ),
                    SizedBox(width: 20),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(EvilIcons.comment, color: Colors.blue),
                        ),
                        // SizedBox(width: 10),
                        Text("58")
                      ],
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(SimpleLineIcons.share_alt, color: Colors.blue),
                )
              ],
            )
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              "It is a long established fact thata reader will be ditracted by the readable content...",
              maxLines: 2,
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.push(context,
                MaterialPageRoute(
                  builder: (context) => AllComment(index: widget.index)
                )
              );
            },
            child: Text("View all comment", style: TextStyle(color: Colors.grey[400])),
          )
        ]
      ),
    );
  }
}  
